import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
  .setTitle('Hotel reservation API')
  .setDescription('The API description')
  .setVersion('1.0')
  .addTag('arenda')
  .build();
const document = SwaggerModule.createDocument(app, config);
SwaggerModule.setup('api', app, document);

  await app.listen(3300);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
