import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NestPgpromiseModule } from 'nest-pgpromise/dist';
import { ArendaModule } from './arenda/arenda.module';

@Module({
  imports: [

    NestPgpromiseModule.register({
      connection: {
        host: 'localhost',
        port: 5432,
        database: 'hotel_reserv',
        user: 'hoteladmin',
        password: 'hoteladmin',
      },
    }),

    ArendaModule,
    
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
