import { Test, TestingModule } from '@nestjs/testing';
import { ArendaController } from './arenda.controller';

describe('ArendaController', () => {
  let controller: ArendaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArendaController],
    }).compile();

    controller = module.get<ArendaController>(ArendaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
