import { Injectable, Inject} from '@nestjs/common';
import { NEST_PGPROMISE_CONNECTION } from 'nestjs-pgpromise';

import { IDatabase } from 'pg-promise';
import  {ReservItem} from './arenda'

import {CreateReservDto  } from './dto/create-reserv.dto'
import {DateRange } from './dto/query-daterange.dto'



@Injectable()
export class ArendaService {
    constructor( @Inject(NEST_PGPROMISE_CONNECTION) private readonly pg: IDatabase<any>) {}


    //Добавление записи в резервирование
    create(crd: CreateReservDto) {

        //'INSERT INTO reserv (id_room, reserv_dates) VALUES (1, '[2022-08-1, 2022-12-31]')
        return this.pg.many(`INSERT INTO reserv (id_room, reserv_dates) VALUES (${crd.id_room}, '[${crd.date_start}, ${crd.date_stop}]') RETURNING *`)
        .then(data  => {
            return data
        })
        .catch(error => {
            console.log(error);
            return {text: (error).toString(), error}
        });
    };

      

    // Запрос Список доступных номеров на выбранные даты 
    getFree(qr: DateRange) {

            //select * from free_rooms('2021-10-10'::date, '2021-10-12'::date)
         return this.pg.many(`select * from free_rooms('${qr.date_start}'::date, '${qr.date_stop}'::date)`)
            .then(data => {
                return data
            })
            .catch(error => {
                return error
            });
    };


    
    // Запрос Отчет средней загрузки номеров (по дням) в заданном интервале с группировкой по месяцам  
    getAvgusedRoom(qr: DateRange) {

        //select * from free_rooms('2021-10-10'::date, '2021-10-12'::date)
     return this.pg.many(`select * from avg_used_rooms('${qr.date_start}'::date, '${qr.date_stop}'::date)`)
        .then(data => {
            return data
        })
        .catch(error => {
            return error
        });
};


    // Возвращает всю аренду
    getAll(): Promise<any[]>  {
     
        return this.pg.many("SELECT * from reserv")
            .then((data: Array<ReservItem>) => {
                return data
            })
            .catch(error => {
                return error
            });
    };


      


}
