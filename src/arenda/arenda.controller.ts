import { Controller, Get, Post, Body , Query} from '@nestjs/common';
import { ApiTags,ApiOperation } from '@nestjs/swagger';
import { ArendaService } from './arenda.service';
import {CreateReservDto  } from './dto/create-reserv.dto'
import {DateRange } from './dto/query-daterange.dto'


@Controller('arenda/api')
export class ArendaController {
    constructor(private readonly arendaService: ArendaService){}

    @Post()
    @ApiTags('arenda')
    @ApiOperation({ summary: 'Добавление записи в резервирование' })
    
    async create(@Body() crd: CreateReservDto) {
        return this.arendaService.create(crd);
    };


    @Get()
    @ApiTags('arenda')
    @ApiOperation({ summary: 'Возвращает все записи аренды' })

    findAll(): Promise<any[]> {
        return  this.arendaService.getAll();
    };

  
    @Get('free')
    @ApiTags('arenda')
    @ApiOperation({ summary: 'Запрос. Свободные номера на диапазон дат' })

    getFree(@Query() query:DateRange) {
      return this.arendaService.getFree(query)
    };
    
    @Get('avg')
    @ApiTags('arenda')
    @ApiOperation({ summary: 'Запрос. Запрос Отчет средней загрузки номеров (по дням) в заданном интервале с группировкой по месяцам' })

    getAvg(@Query() query:DateRange) {
      return this.arendaService.getAvgusedRoom(query)
    };


}
