import { Test, TestingModule } from '@nestjs/testing';
import { ArendaService } from './arenda.service';

describe('ArendaService', () => {
  let service: ArendaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArendaService],
    }).compile();

    service = module.get<ArendaService>(ArendaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
