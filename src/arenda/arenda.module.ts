import { Module } from '@nestjs/common';
import { ArendaController } from './arenda.controller';
import { ArendaService } from './arenda.service';

@Module({
  controllers: [ArendaController],
  providers: [ArendaService]
})
export class ArendaModule {}
