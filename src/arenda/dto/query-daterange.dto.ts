import { IsDate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';


export class DateRange {
    
@IsDate()
@ApiProperty({
  type: Date,
  description: 'Дата начала аренды',
  default: new Date().toISOString().split("T")[0]
})
  readonly date_start: Date;

@IsDate()
@ApiProperty({
  type: Date,
  description: 'Дата окончания аренды',
  default: new Date().toISOString().split("T")[0]
  
  
})
  readonly date_stop: Date;


}
