import { IsInt, IsDate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateReservDto {

  @IsInt()
  @ApiProperty({
    description: 'id Room',
    minimum: 1,
    default: 1
  })
  readonly id_room: number;

  @IsDate()
  @ApiProperty({
    type: Date,
    description: 'Дата начала аренды',
    default: new Date().toISOString().split("T")[0]
  })
  readonly date_start: Date;

@IsDate()
@ApiProperty({
  type: Date,
  description: 'Дата окончания аренды',
  default: new Date().toISOString().split("T")[0]
})
  readonly date_stop: Date;


}
