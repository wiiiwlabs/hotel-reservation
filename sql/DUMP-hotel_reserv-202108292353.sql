--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

-- Started on 2021-08-29 23:53:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 29512)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;
CREATE EXTENSION btree_gist;

--
-- TOC entry 3298 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 405 (class 1255 OID 30199)
-- Name: avg_used_rooms(date, date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.avg_used_rooms(begin_day date, end_day date) RETURNS TABLE(id_room integer, month_name character varying, month_num integer, day_avg double precision)
    LANGUAGE sql
    AS $$
with 

--сгенирим месяца
mngs as (

	select 
		daterange(m::date, (m + interval '1 month' - interval '1 day')::date, '[]') as mnrg,		--соберем daterange  с 1 по последний день для каждого месяца
		trim(TO_CHAR(m::date,  'month')) as month_name,													--название месяца
		date_part('month', m::date) as month_num															--номер месяца 
    from generate_series( begin_day, end_day, '1 month') m
                    
),


t2 as (
	select *, 
	
	upper(reserv_dates)-lower(reserv_dates) as full_count_day, -- полное количество дней аренды
	
	-- разберем количество дней аренды на каждый месяц
	case 
		when upper(reserv_dates) >= upper(m.mnrg) and lower(reserv_dates) <= lower(m.mnrg) then upper(m.mnrg) - lower(m.mnrg)
		when upper(reserv_dates) >= upper(m.mnrg) and lower(reserv_dates) >= lower(m.mnrg) then upper(m.mnrg) - lower(reserv_dates)
		when upper(reserv_dates) <= upper(m.mnrg) and lower(reserv_dates) >= lower(m.mnrg) then upper(reserv_dates) - lower(reserv_dates)  
		when upper(reserv_dates) <= upper(m.mnrg) and lower(reserv_dates) <= lower(m.mnrg) then upper(reserv_dates) -  lower(m.mnrg)
	
	end as dayinmon
		
	from reserv r
	left join mngs m 
	on  r.reserv_dates  &&   m.mnrg
),


res as (
	select id_room, month_name, month_num,  avg(dayinmon) as day_avg  from t2
	group by id_room, month_name, month_num
	order by month_num, id_room
)


--select * from t2
select * from res where day_avg is not null

 	
$$;


--
-- TOC entry 398 (class 1255 OID 30136)
-- Name: end_of_month(date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.end_of_month(date) RETURNS date
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
	select (date_trunc('month', $1) + interval '1 month' - interval '1 day')::date;
$_$;


--
-- TOC entry 402 (class 1255 OID 30197)
-- Name: free_rooms(date, date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.free_rooms(begin_day date, end_day date) RETURNS TABLE(id_room integer, room_number integer, price double precision)
    LANGUAGE sql
    AS $$
 (select r.id_room, r.room_number, r.price from rooms r 
  left join (SELECT * FROM reserv r where (r.reserv_dates && daterange(begin_day, end_day,'[]'))) t
on r.id_room = t.id_room
where t.id_room is null)
 	
$$;


--
-- TOC entry 399 (class 1255 OID 30137)
-- Name: random_date_in_range(date, date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.random_date_in_range(date, date) RETURNS date
    LANGUAGE sql
    AS $_$
    SELECT $1 + floor( ($2 - $1 + 1) * random() )::INTEGER;
$_$;


--
-- TOC entry 400 (class 1255 OID 30138)
-- Name: random_in_range(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.random_in_range(integer, integer) RETURNS integer
    LANGUAGE sql
    AS $_$
    SELECT floor(($1 + ($2 - $1 + 1) * random()))::INTEGER;
$_$;


--
-- TOC entry 401 (class 1255 OID 30139)
-- Name: random_timestamp_in_range(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.random_timestamp_in_range(timestamp without time zone, timestamp without time zone) RETURNS timestamp without time zone
    LANGUAGE sql
    AS $_$
    SELECT $1 + floor( 
      ( extract(epoch FROM $2 - $1) + 1) * random()
    )::INTEGER::TEXT::INTERVAL;
$_$;


--
-- TOC entry 403 (class 1255 OID 30140)
-- Name: tgf_reserv_bi_calc_amount(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.tgf_reserv_bi_calc_amount() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE 
	
   	countday int; 		-- количество дней аренды
  	rprice float;		-- стоимость номера
   	fullamount float;	-- стоимость аренды
  	saleval int[]; 		-- [массив ид_скидки, размер скидки]
  
       
begin

	--raise notice 'upper_inc(new.rng) calc amount %: %', new.rng, upper_inc(new.rng);

	--определим количество забронированых дней
	countday = upper(new.reserv_dates) - lower(new.reserv_dates);


	--получим стоимость аренды номера
	rprice = (select r.price from rooms r where r.id_room = new.id_room);

	--определим id_скидки и скидку  для сохранения
	saleval = (SELECT ARRAY[id_sale::int, sale::int] FROM sales where day_count <= countday order by 1 desc limit 1);
	

	--расчитаем Полную стоимость  = Прайс * На количество дней
	fullamount = rprice * countday;


	--если скидку не получили 
	if saleval is null then 
		new.amount = fullamount; 
	else 
		--Сохраним ид скидки
		new.id_sale = saleval[1]::int;

		-- Стоимость = (Полная стоимость) - (Полная стоимость /100 * Процент скидки)
		new.amount = fullamount - (fullamount / 100 * saleval[2]::int) ;
	end if;


	--RAISE NOTICE  'fullamount: %  saleval: % countday: % rprice: % ,new.amount: %',  fullamount, saleval, countday, rprice, new.amount ;
 	

	return new;

end
$$;


--
-- TOC entry 3299 (class 0 OID 0)
-- Dependencies: 403
-- Name: FUNCTION tgf_reserv_bi_calc_amount(); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION public.tgf_reserv_bi_calc_amount() IS 'Расчет скидки с 10 дней - 10% скидка свыше 20 дней - 20% скидка. Значения скидок хранятся в таблице day_sales';


--
-- TOC entry 404 (class 1255 OID 30141)
-- Name: tgf_reserv_bi_chwd(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.tgf_reserv_bi_chwd() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE 

   msg text;		--сообщение 	
   startday int; 	--номер дня недели начала аренды
   finishday int;	--номер дня недели окончания аренды
   worngdays int[]:= array(select id_worngday from worng_days);		-- массив запрещенных дней
       
begin

	--raise notice 'upper_inc(new.rng) %: %', new.rng, upper_inc(new.rng);

	startday = extract(dow from lower(new.reserv_dates));
	finishday = extract(dow from upper(new.reserv_dates))-1; 

	--RAISE NOTICE  'startday: %  finishday: % lower(new.rng): % upper(new.rng): %',  startday, finishday, lower(new.rng), upper(new.rng) ;

	if (SELECT startday = ANY(worngdays::int[])) or  (SELECT finishday = ANY(worngdays::int[])) then
		msg:= (select string_agg(desk, ',')  from worng_days);
    	RAISE EXCEPTION   'Выберите другой день! Въезд и выезд не могут попадать на %!', msg;
       	return null;
     else 
      	return new;
    END IF;
  
end
$$;


--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 404
-- Name: FUNCTION tgf_reserv_bi_chwd(); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION public.tgf_reserv_bi_chwd() IS 'Проверка на понедельник четверг въезд и выезд не могут попадать на понедельник и четверг';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 201 (class 1259 OID 30142)
-- Name: reserv; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reserv (
    id_reserv integer NOT NULL,
    id_room integer NOT NULL,
    id_sale integer,
    amount double precision NOT NULL,
    reserv_dates daterange NOT NULL
);


--
-- TOC entry 202 (class 1259 OID 30148)
-- Name: reserv_id_reserv_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reserv_id_reserv_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 202
-- Name: reserv_id_reserv_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reserv_id_reserv_seq OWNED BY public.reserv.id_reserv;


--
-- TOC entry 203 (class 1259 OID 30150)
-- Name: rooms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rooms (
    id_room integer NOT NULL,
    room_number integer NOT NULL,
    price double precision DEFAULT 1000 NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 30154)
-- Name: rooms_id_room_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rooms_id_room_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 204
-- Name: rooms_id_room_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rooms_id_room_seq OWNED BY public.rooms.id_room;


--
-- TOC entry 205 (class 1259 OID 30156)
-- Name: sales; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sales (
    id_sale integer NOT NULL,
    desk character varying(100) NOT NULL,
    day_count integer NOT NULL,
    sale integer NOT NULL
);


--
-- TOC entry 206 (class 1259 OID 30159)
-- Name: sales_id_sale_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sales_id_sale_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 206
-- Name: sales_id_sale_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sales_id_sale_seq OWNED BY public.sales.id_sale;


--
-- TOC entry 207 (class 1259 OID 30161)
-- Name: worng_days; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.worng_days (
    id_worngday integer,
    desk character varying(100)
);


--
-- TOC entry 3132 (class 2604 OID 30164)
-- Name: reserv id_reserv; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reserv ALTER COLUMN id_reserv SET DEFAULT nextval('public.reserv_id_reserv_seq'::regclass);


--
-- TOC entry 3134 (class 2604 OID 30165)
-- Name: rooms id_room; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rooms ALTER COLUMN id_room SET DEFAULT nextval('public.rooms_id_room_seq'::regclass);


--
-- TOC entry 3135 (class 2604 OID 30166)
-- Name: sales id_sale; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sales ALTER COLUMN id_sale SET DEFAULT nextval('public.sales_id_sale_seq'::regclass);


--
-- TOC entry 3286 (class 0 OID 30142)
-- Dependencies: 201
-- Data for Name: reserv; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.reserv VALUES (2, 10, 1, 9000, '[2021-08-01,2021-08-11)');
INSERT INTO public.reserv VALUES (9, 6, NULL, 4000, '[2021-08-03,2021-08-07)');
INSERT INTO public.reserv VALUES (10, 22, 1, 12600, '[2021-08-01,2021-08-15)');
INSERT INTO public.reserv VALUES (14, 15, NULL, 8000, '[2021-08-03,2021-08-11)');
INSERT INTO public.reserv VALUES (17, 12, 2, 20800, '[2021-08-03,2021-08-29)');
INSERT INTO public.reserv VALUES (19, 14, 2, 23200, '[2021-08-03,2021-09-01)');
INSERT INTO public.reserv VALUES (20, 2, 1, 9900, '[2021-08-03,2021-08-14)');
INSERT INTO public.reserv VALUES (23, 4, 1, 10800, '[2021-08-03,2021-08-15)');
INSERT INTO public.reserv VALUES (26, 18, NULL, 7000, '[2021-08-01,2021-08-08)');
INSERT INTO public.reserv VALUES (29, 8, 2, 17600, '[2021-08-01,2021-08-23)');
INSERT INTO public.reserv VALUES (30, 20, NULL, 9000, '[2021-08-03,2021-08-12)');
INSERT INTO public.reserv VALUES (38, 9, 1, 9900, '[2021-08-03,2021-08-14)');
INSERT INTO public.reserv VALUES (40, 24, NULL, 2000, '[2021-08-03,2021-08-05)');
INSERT INTO public.reserv VALUES (47, 5, 1, 13500, '[2021-08-01,2021-08-16)');
INSERT INTO public.reserv VALUES (54, 17, NULL, 8000, '[2021-08-01,2021-08-09)');
INSERT INTO public.reserv VALUES (91, 7, 2, 20000, '[2021-08-01,2021-08-26)');
INSERT INTO public.reserv VALUES (105, 23, 2, 24800, '[2021-08-01,2021-09-01)');
INSERT INTO public.reserv VALUES (136, 3, 1, 11700, '[2021-08-01,2021-08-14)');
INSERT INTO public.reserv VALUES (173, 11, 1, 13500, '[2021-08-03,2021-08-18)');
INSERT INTO public.reserv VALUES (184, 13, 2, 20000, '[2021-08-01,2021-08-26)');
INSERT INTO public.reserv VALUES (187, 16, 1, 9900, '[2021-08-03,2021-08-14)');
INSERT INTO public.reserv VALUES (194, 21, 2, 20000, '[2021-08-03,2021-08-28)');
INSERT INTO public.reserv VALUES (261, 19, 2, 17600, '[2021-08-03,2021-08-25)');
INSERT INTO public.reserv VALUES (1008, 17, 1, 10800, '[2021-08-11,2021-08-23)');
INSERT INTO public.reserv VALUES (1009, 18, 1, 13500, '[2021-08-10,2021-08-25)');
INSERT INTO public.reserv VALUES (1012, 24, 2, 20800, '[2021-08-11,2021-09-06)');
INSERT INTO public.reserv VALUES (1024, 15, 2, 17600, '[2021-08-11,2021-09-02)');
INSERT INTO public.reserv VALUES (1164, 10, 2, 23200, '[2021-08-11,2021-09-09)');
INSERT INTO public.reserv VALUES (1269, 6, 1, 16200, '[2021-08-10,2021-08-28)');
INSERT INTO public.reserv VALUES (2011, 22, 2, 16000, '[2021-08-15,2021-09-04)');
INSERT INTO public.reserv VALUES (2023, 2, 2, 18400, '[2021-08-17,2021-09-09)');
INSERT INTO public.reserv VALUES (2032, 4, NULL, 2000, '[2021-08-17,2021-08-19)');
INSERT INTO public.reserv VALUES (2035, 20, NULL, 6000, '[2021-08-15,2021-08-21)');
INSERT INTO public.reserv VALUES (2044, 16, 1, 13500, '[2021-08-15,2021-08-30)');
INSERT INTO public.reserv VALUES (2067, 3, 2, 17600, '[2021-08-15,2021-09-06)');
INSERT INTO public.reserv VALUES (2098, 9, 1, 15300, '[2021-08-15,2021-09-01)');
INSERT INTO public.reserv VALUES (2112, 5, 2, 23200, '[2021-08-17,2021-09-15)');
INSERT INTO public.reserv VALUES (3009, 11, 1, 16200, '[2021-08-21,2021-09-08)');
INSERT INTO public.reserv VALUES (3013, 4, 2, 17600, '[2021-08-20,2021-09-11)');
INSERT INTO public.reserv VALUES (3107, 20, 2, 16800, '[2021-08-21,2021-09-11)');
INSERT INTO public.reserv VALUES (4004, 18, 1, 16200, '[2021-08-25,2021-09-12)');
INSERT INTO public.reserv VALUES (4007, 8, 1, 10800, '[2021-08-25,2021-09-06)');
INSERT INTO public.reserv VALUES (4008, 7, 2, 26400, '[2021-08-27,2021-09-29)');
INSERT INTO public.reserv VALUES (4061, 17, 2, 36000, '[2021-08-27,2021-10-11)');
INSERT INTO public.reserv VALUES (4065, 13, 2, 26400, '[2021-08-27,2021-09-29)');
INSERT INTO public.reserv VALUES (4359, 19, 2, 37600, '[2021-08-27,2021-10-13)');
INSERT INTO public.reserv VALUES (5007, 6, 2, 40000, '[2021-08-31,2021-10-20)');
INSERT INTO public.reserv VALUES (5022, 16, NULL, 5000, '[2021-09-01,2021-09-06)');
INSERT INTO public.reserv VALUES (5024, 9, 2, 42400, '[2021-09-01,2021-10-24)');
INSERT INTO public.reserv VALUES (5028, 12, 2, 41600, '[2021-09-01,2021-10-23)');
INSERT INTO public.reserv VALUES (5083, 21, 1, 16200, '[2021-08-31,2021-09-18)');
INSERT INTO public.reserv VALUES (5120, 23, 1, 15300, '[2021-09-01,2021-09-18)');
INSERT INTO public.reserv VALUES (5192, 14, 2, 25600, '[2021-09-01,2021-10-03)');
INSERT INTO public.reserv VALUES (6041, 1, NULL, 2000, '[2022-01-07,2022-01-09)');
INSERT INTO public.reserv VALUES (6043, 25, NULL, 1000, '[2021-08-29,2021-08-30)');
INSERT INTO public.reserv VALUES (6045, 1, NULL, 1000, '[2021-08-29,2021-08-30)');


--
-- TOC entry 3288 (class 0 OID 30150)
-- Dependencies: 203
-- Data for Name: rooms; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.rooms VALUES (1, 101, 1000);
INSERT INTO public.rooms VALUES (2, 102, 1000);
INSERT INTO public.rooms VALUES (3, 103, 1000);
INSERT INTO public.rooms VALUES (4, 104, 1000);
INSERT INTO public.rooms VALUES (5, 105, 1000);
INSERT INTO public.rooms VALUES (6, 106, 1000);
INSERT INTO public.rooms VALUES (7, 107, 1000);
INSERT INTO public.rooms VALUES (8, 108, 1000);
INSERT INTO public.rooms VALUES (9, 109, 1000);
INSERT INTO public.rooms VALUES (10, 110, 1000);
INSERT INTO public.rooms VALUES (11, 201, 1000);
INSERT INTO public.rooms VALUES (12, 202, 1000);
INSERT INTO public.rooms VALUES (13, 203, 1000);
INSERT INTO public.rooms VALUES (14, 204, 1000);
INSERT INTO public.rooms VALUES (15, 205, 1000);
INSERT INTO public.rooms VALUES (16, 206, 1000);
INSERT INTO public.rooms VALUES (17, 207, 1000);
INSERT INTO public.rooms VALUES (18, 208, 1000);
INSERT INTO public.rooms VALUES (19, 209, 1000);
INSERT INTO public.rooms VALUES (20, 210, 1000);
INSERT INTO public.rooms VALUES (21, 211, 1000);
INSERT INTO public.rooms VALUES (22, 212, 1000);
INSERT INTO public.rooms VALUES (23, 213, 1000);
INSERT INTO public.rooms VALUES (24, 214, 1000);
INSERT INTO public.rooms VALUES (25, 215, 1000);


--
-- TOC entry 3290 (class 0 OID 30156)
-- Dependencies: 205
-- Data for Name: sales; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sales VALUES (1, 'Скидка 10%', 10, 10);
INSERT INTO public.sales VALUES (2, 'Скидка 20%', 20, 20);


--
-- TOC entry 3292 (class 0 OID 30161)
-- Dependencies: 207
-- Data for Name: worng_days; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.worng_days VALUES (1, 'Понедельник');
INSERT INTO public.worng_days VALUES (4, 'Четверг');


--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 202
-- Name: reserv_id_reserv_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.reserv_id_reserv_seq', 6047, true);


--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 204
-- Name: rooms_id_room_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.rooms_id_room_seq', 2, true);


--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 206
-- Name: sales_id_sale_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sales_id_sale_seq', 1, false);


--
-- TOC entry 3137 (class 2606 OID 30168)
-- Name: reserv reserv_id_room_rng_excl; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reserv
    ADD CONSTRAINT reserv_id_room_rng_excl EXCLUDE USING gist (id_room WITH =, reserv_dates WITH &&);


--
-- TOC entry 3139 (class 2606 OID 30170)
-- Name: reserv reserv_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reserv
    ADD CONSTRAINT reserv_pkey PRIMARY KEY (id_reserv);


--
-- TOC entry 3141 (class 2606 OID 30172)
-- Name: rooms rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id_room);


--
-- TOC entry 3143 (class 2606 OID 30174)
-- Name: rooms rooms_room_number_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_room_number_key UNIQUE (room_number);


--
-- TOC entry 3145 (class 2606 OID 30176)
-- Name: sales sales_day_count_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sales
    ADD CONSTRAINT sales_day_count_key UNIQUE (day_count);


--
-- TOC entry 3147 (class 2606 OID 30178)
-- Name: sales sales_day_sale_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sales
    ADD CONSTRAINT sales_day_sale_key UNIQUE (sale);


--
-- TOC entry 3149 (class 2606 OID 30180)
-- Name: sales sales_desk_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sales
    ADD CONSTRAINT sales_desk_key UNIQUE (desk);


--
-- TOC entry 3151 (class 2606 OID 30182)
-- Name: sales sales_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sales
    ADD CONSTRAINT sales_pkey PRIMARY KEY (id_sale);


--
-- TOC entry 3154 (class 2620 OID 30183)
-- Name: reserv tg_reserv_bi_calc_amount; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tg_reserv_bi_calc_amount BEFORE INSERT ON public.reserv FOR EACH ROW EXECUTE FUNCTION public.tgf_reserv_bi_calc_amount();


--
-- TOC entry 3155 (class 2620 OID 30184)
-- Name: reserv tg_reserv_bi_chwd; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tg_reserv_bi_chwd BEFORE INSERT ON public.reserv FOR EACH ROW EXECUTE FUNCTION public.tgf_reserv_bi_chwd();


--
-- TOC entry 3152 (class 2606 OID 30185)
-- Name: reserv reserv_id_room_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reserv
    ADD CONSTRAINT reserv_id_room_fkey FOREIGN KEY (id_room) REFERENCES public.rooms(id_room);


--
-- TOC entry 3153 (class 2606 OID 30190)
-- Name: reserv reserv_id_sale_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reserv
    ADD CONSTRAINT reserv_id_sale_fkey FOREIGN KEY (id_sale) REFERENCES public.sales(id_sale);


-- Completed on 2021-08-29 23:53:14

--
-- PostgreSQL database dump complete
--

