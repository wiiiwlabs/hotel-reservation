--Отчет средней загрузки номеров (по дням) в заданном интервале с группировкой по месяцам 

--DROP FUNCTION avg_used_rooms(date,date);

create or replace function avg_used_rooms(begin_day date, end_day date) returns table (id_room int, month_name varchar, month_num int, day_avg float )
as
$body$
with 

--сгенирим месяца
mngs as (

	select 
		daterange(m::date, (m + interval '1 month' - interval '1 day')::date, '[]') as mnrg,		--соберем daterange  с 1 по последний день для каждого месяца
		trim(TO_CHAR(m::date,  'month')) as month_name,													--название месяца
		date_part('month', m::date) as month_num															--номер месяца 
    from generate_series( begin_day, end_day, '1 month') m
                    
),


t2 as (
	select *, 
	
	upper(reserv_dates)-lower(reserv_dates) as full_count_day, -- полное количество дней аренды
	
	-- разберем количество дней аренды на каждый месяц
	case 
		when upper(reserv_dates) >= upper(m.mnrg) and lower(reserv_dates) <= lower(m.mnrg) then upper(m.mnrg) - lower(m.mnrg)
		when upper(reserv_dates) >= upper(m.mnrg) and lower(reserv_dates) >= lower(m.mnrg) then upper(m.mnrg) - lower(reserv_dates)
		when upper(reserv_dates) <= upper(m.mnrg) and lower(reserv_dates) >= lower(m.mnrg) then upper(reserv_dates) - lower(reserv_dates)  
		when upper(reserv_dates) <= upper(m.mnrg) and lower(reserv_dates) <= lower(m.mnrg) then upper(reserv_dates) -  lower(m.mnrg)
	
	end as dayinmon
		
	from reserv r
	left join mngs m 
	on  r.reserv_dates  &&   m.mnrg
),


res as (
	select id_room, month_name, month_num,  avg(dayinmon) as day_avg  from t2
	group by id_room, month_name, month_num
	order by month_num, id_room
)


--select * from t2
select * from res where day_avg is not null

 	
$body$
language sql;



select * from avg_used_rooms('2021-01-1'::date, '2021-12-31'::date )

