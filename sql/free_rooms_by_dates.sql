
--•	Список доступных номеров на выбранные даты 

--комната 1 занята с 2021-08-1, по 2021-12-31
--комната 25 нет значений

--
--@set begin_day = '2021-10-10'::date
--@set end_day = '2021-10-12'::date
--
--select r.id_room, r.room_number, r.price from rooms r 
--  left join (SELECT * FROM reserv r where (r.reserv_dates && daterange(:begin_day,:end_day,'[]'))) t
--on r.id_room = t.id_room
--where t.id_room is null
--


create or replace function free_rooms(begin_day date, end_day date) returns table (id_room int, room_number int, price float )
as
$body$
 (select r.id_room, r.room_number, r.price from rooms r 
  left join (SELECT * FROM reserv r where (r.reserv_dates && daterange(begin_day, end_day,'[]'))) t
on r.id_room = t.id_room
where t.id_room is null)
 	
$body$
language sql;


select * from free_rooms('2021-10-10'::date, '2021-10-12'::date)




