
-----------------------------------------------------------

@set dmax = '60'::int					-- макс. количестово дней диапазона для генерации 
@SET start_room = '1'::int				-- начальная комната
@SET end_room = '25'::int				-- конечная комната
@set countseries = '1000'::int			-- количество серий
@set begin_date = '2021-08-30'::date	-- дата от которой генирируем

-------------------------------------------------------------

create or replace  FUNCTION random_date_in_range(DATE, DATE)
RETURNS DATE
LANGUAGE SQL
AS $$
    SELECT $1 + floor( ($2 - $1 + 1) * random() )::INTEGER;
$$;


CREATE OR REPLACE function random_in_range(INTEGER, INTEGER) RETURNS INTEGER
AS $$
    SELECT floor(($1 + ($2 - $1 + 1) * random()))::INTEGER;
$$ LANGUAGE SQL;


-------------------------------------------------------------


INSERT INTO reserv (id_room, reserv_dates)

	SELECT  random_in_range(:start_room, :end_room) as id_room, 
		
		daterange (
			random_date_in_range(:begin_date,	:begin_date + integer '2'), 				-- random для начальной даты
			random_date_in_range(:begin_date + integer '3', :begin_date + integer :dmax),	-- random для конечной даты
		'[]') as rng
		
		FROM generate_series(1, :countseries) AS s(a)
		
ON CONFLICT DO NOTHING;		--игнорируем RAISE т.к. в серии могут быть повторы и сработки тригеров

-----------------------------------------------------------


-- ручная вставка 
-- INSERT INTO reserv (id_room, rng) VALUES (1, '[2021-08-1, 2021-12-31]');


--комната 1 занята с 2021-08-1, по 2021-12-31
--комната 25 нет значений