**Тестовое задание: Бронирование номеров**

**Реализовать запросы:**

- Список доступных номеров на выбранные даты
- Забронировать номер
- Отчет средней загрузки номеров (по дням) в заданном интервале с группировкой по месяцам
- Посеять 50+ бронирований

**Условия:**

* въезд и выезд не могут попадать на понедельник и четверг
* цена за номер 1000 руб
* с 10 дней - 10% скидка
* свыше 20 дней - 20% скидка

**Требование к разработке:**

* язык: TypeScript
* платформа: NodeJs 12+
* фреймворк: NestJs 7+
* субд: Postgres 9+ 
* без использования ORM библиотек (чистый sql).
* Описание процесса запуска в readme.md

**Результат:**

* git 
* swagger (и моки, если без бэкенд)
* ER Diagram  
* Unit-test 



### Реализация:
---

**Для установки проекта необходимо:** 

Клонировать репозитарий:

    git clone https://wiiiwlabs@bitbucket.org/wiiiwlabs/hotel-reservation.git
    
    cd hotel-reservation

Установить зависимости:
    
    npm i

Загрузить дамп базы данных на СУБД Postgres:

    ./sql/DUMP-hotel_reserv-202108292353.sql

В файле `./src/app.module.ts` настроить доступ к СУБД Postgres:
 
 
         connection: {
           host: 'localhost',
           port: 5432,
           database: 'hotel_reserv',
           user: 'hoteladmin',
           password: 'hoteladmin',
         }
 


Запустить сборку:

    npm run start

Перейти по ссылке: [http://localhost:3300](http://localhost:3300)

Swagger по адресу: [http://localhost:3300/api/](http://localhost:3300/api)


---
**Описание таблиц:**
В приложении используются следующие таблицы:

- **reserv** - Данные о аренде 
    - id_reserv  *(id)*
    - id_room  *(Внешний ключ на комнату)*
    - id_sale  *(Внешний ключ на скидку)*
    - amount  *(Стоимость аренды со скидкой, за все дни аренды)*
    - reserv_dates - *(Диапазонный тип, храним дату начала и окончания аренды)*


- **rooms** - Данные о комнатах (id_room, room_number, price)
    - id_room  *(id)* 
    - room_number *(номер комнаты (возможно сделать текстовым, для хранения префикса типа 101А))*, 
    - price  *(стоимость аренды номера т.е. для каждого номера можно устанавливать собственную стоимость аренды, что ближе к реальным условиям. По умолчанию 1000)*


- **sales** - Данные о скидках (id_sale,desk,day_count,sale) - *Количество скидок может быть любым*
    - id_sale *(id)*
    - desk - *(Текстовое описание скидки)*
    - day_count *(Количество дней для которых применяется скидка)*
    - sale *(Процент скидки)*


- **worng_days** - Данные о стоп днях (понедельник, четверг) - *Количество дней может быть любым, (<=7)*
    - id_worngday  *(Порядковый номер дня)*
    - desk *(Название дня)*

---

Для обеспечения уникальности таблицы **reserv** используется расширение **btree_gist** 
и составной ключ `(EXCLUDE USING GIST (id_room WITH =, reserv_dates WITH &&)` на **id_room** комнаты и временного диапазона **reserv_dates** аренды.

**Вся бизнес-логика приложения сосредоточена в процедурах/функциях и триггерах базы данных.**

Функция *tgf_reserv_bi_calc_amount* Задача: ***Расчет стоимости номера***


    CREATE OR REPLACE FUNCTION public.tgf_reserv_bi_calc_amount()
    RETURNS trigger
    LANGUAGE plpgsql
    AS $function$
    
    DECLARE 
    	
    countday int; 		-- количество дней аренды
  	rprice float;		-- стоимость номера
   	fullamount float;	-- стоимость аренды
  	saleval int[]; 		-- [массив ид_скидки, размер скидки]
         
    begin

	--определим количество забронированых дней
	countday = upper(new.reserv_dates) - lower(new.reserv_dates);

	--получим стоимость аренды номера
	rprice = (select r.price from rooms r where r.id_room = new.id_room);

	--определим id_скидки и скидку  для сохранения
	saleval = (SELECT ARRAY[id_sale::int, sale::int] FROM sales where day_count <= countday order by 1 desc limit 1);

	--расчитаем Полную стоимость  = Прайс * На количество дней
	fullamount = rprice * countday;


	--если скидку не получили 
	if saleval is null then 
		new.amount = fullamount; 
	else 
		--Сохраним ид скидки
		new.id_sale = saleval[1]::int;

		-- Стоимость = (Полная стоимость) - (Полная стоимость /100 * Процент скидки)
		new.amount = fullamount - (fullamount / 100 * saleval[2]::int) ;
	end if;
	return new;
    end
    $function$
    ;


---

Функция *tgf_reserv_bi_chwd* Задача: ***Въезд и выезд не могут попадать на понедельник и четверг***


    CREATE OR REPLACE FUNCTION public.tgf_reserv_bi_chwd()
    RETURNS trigger
    LANGUAGE plpgsql
    AS $function$

    DECLARE 

    msg text;		--сообщение 	
    startday int; 	--номер дня недели начала аренды
    finishday int;	--номер дня недели окончания аренды
    worngdays int[]:= array(select id_worngday from worng_days);	-- массив стоп дней
        
    begin

        startday = extract(dow from lower(new.reserv_dates));
        finishday = extract(dow from upper(new.reserv_dates))-1; 

        if (SELECT startday = ANY(worngdays::int[])) or  (SELECT finishday = ANY(worngdays::int[])) then
            msg:= (select string_agg(desk, ',')  from worng_days);
            RAISE EXCEPTION   'Выберите другой день! Въезд и выезд не могут попадать на %!', msg;
            return null;
        else 
            return new;
        END IF;
    
    end
    $function$
    ;

---

**Данные по запросам возвращаются через:**

функцию **free_rooms**(begin_day date, end_day date) - свободные номера на указанный диапазон дат -  в таблице с полями:

 - id_room int, *(id номера)*
 - room_number int, *(наименование номера)*
 - price float *(стоимость номера)*
---
    create or replace function free_rooms(begin_day date, end_day date) returns table (id_room int, room_number int, price float )
    as
    $body$
     
     (select r.id_room, r.room_number, r.price from rooms r 
     left join (SELECT * FROM reserv r where (r.reserv_dates && daterange(begin_day, end_day,'[]'))) t
     on r.id_room = t.id_room
     where t.id_room is null)
 	
    $body$
    language sql;

*Пример запроса:*
    select * from free_rooms('2021-10-10'::date, '2021-10-12'::date)


----



функцию **avg_used_rooms**(begin_day date, end_day date) - Отчет средней загрузки номеров (по дням) в заданном интервале с группировкой по месяцам - в таблице с полями:

   - id_room int, *(id номера)*
   - month_name varchar, *(название месяца)*
   - month_num int, *(номер месяца)*
   - day_avg float *(средняя загрузка по дням месяца)*
---
     create or replace function avg_used_rooms(begin_day date, end_day date) returns table (id_room int, month_name varchar, month_num int, day_avg float )
     as
     $body$
     with 
     --построим список месяцев входящих в заданный диапазон
     mngs as (

	select 
		daterange(m::date, (m + interval '1 month' - interval '1 day')::date, '[]') as mnrg,		--соберем daterange  с 1 по последний день для каждого месяца
		trim(TO_CHAR(m::date,  'month')) as month_name,													--название месяца
		date_part('month', m::date) as month_num															--номер месяца 
    from generate_series( begin_day, end_day, '1 month') m
    ),
    t2 as (
	    select *, 
	
	    upper(reserv_dates)-lower(reserv_dates) as full_count_day, -- полное количество дней аренды
	
	   -- разберем количество дней аренды на каждый месяц
	   case 
		 when upper(reserv_dates) >= upper(m.mnrg) and lower(reserv_dates) <= lower(m.mnrg) then upper(m.mnrg) - lower(m.mnrg)
		 when upper(reserv_dates) >= upper(m.mnrg) and lower(reserv_dates) >= lower(m.mnrg) then upper(m.mnrg) - lower(reserv_dates)
		 when upper(reserv_dates) <= upper(m.mnrg) and lower(reserv_dates) >= lower(m.mnrg) then upper(reserv_dates) - lower(reserv_dates)  
		 when upper(reserv_dates) <= upper(m.mnrg) and lower(reserv_dates) <= lower(m.mnrg) then upper(reserv_dates) -  lower(m.mnrg)
      end as dayinmon
		
	from reserv r
	left join mngs m 
	on  r.reserv_dates  &&   m.mnrg
    ),

    res as (
	    select id_room, month_name, month_num,  avg(dayinmon) as day_avg  from t2
	    group by id_room, month_name, month_num
	    order by month_num, id_room
    )


    --select * from t2
    select * from res where day_avg is not null

 	
    $body$
    language sql;


*Пример запроса:*
    select * from avg_used_rooms('2021-01-1'::date, '2021-12-31'::date )

---


**Дополнительно:**

В папке ./sql расположены следующие sql-скрипты:

**avg_used_rooms.sql**  - *Запрос Отчет средней загрузки номеров (по дням) в заданном интервале с группировкой по месяцам*        

**DUMP-hotel_reserv-202108292353.sql** *Дамп базы данных*

**free_rooms_by_dates.sql** *Список доступных номеров на выбранные даты*

**insert_reserv.sql** *Забронировать номер* 

**reserv_maker.sql** *Генератор бронирований*